import React from "react";
import ReactDOM from "react-dom";
import "./asset/scss/main.scss";
import "./asset/scss/rtl.scss";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
